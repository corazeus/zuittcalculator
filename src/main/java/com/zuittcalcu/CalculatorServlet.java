package com.zuittcalcu;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2021770447197369251L;
	
	public void init() throws ServletException{
		System.out.println("*****************************************");
		System.out.println(" Calculator Servlet has been initialized ");
		System.out.println("*****************************************");
	}
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		out.println("<h1> You are now using the calculator app </h1>");
		out.println("<p> To use the app, input two numbers and an operation. </p>");
		out.println("<p> Hit the submit button after filling in the deatils. </p>");
		out.println("<p> You will get the result shown in your browser!. </p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		System.out.println("Currently in calculator screen");
		
		PrintWriter out = res.getWriter();
		
		String operation = req.getParameter("operation");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int result = 0;
		
		
		if(operation.equals("add")) {
			
			result = num1 + num2;
			
		}else if(operation.equals("subtract")) {
			
			result = num1 - num2;
			
		}else if(operation.equals("multiply")) {
			
			result = num1 * num2;
			
		}else if(operation.equals("divide")){
			
			result = num1 / num2;
			
		}else {
			out.println("<h1> You have entered a wrong operation, try again! </h1>");
		}
		
		out.println("<p> The two numbers you provided are: " +num1+ ", " +num2+ "</p>");
		out.println("<p> The operation you wanted is: " +operation+ "</p>");
		out.println("<p> The result is: " +result+ "</p>");
		
	}
	
	public void destroy() {
		System.out.println("*****************************************");
		System.out.println("  Calculator Servlet has been destroyed  ");
		System.out.println("*****************************************");
	}

}
